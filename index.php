<!-- 
PHP CODE
========
-->
<?


$LINKS = array(
  "Students" => array(
    array(
      "title" => "Moodle",
      "slug" => array("moodle",),
      "url" => "http://moodle.ucl.ac.uk/", 
    ),
    array(
      "title" => "Online Timetable",
      "slug" => array("timetable",),
      "url" => "http://ucl.ac.uk/timetable/", 
    ),
    array(
      "title" => "Portico",
      "slug" => array("portico"),
      "url" => "https://evision.ucl.ac.uk/urd/sits.urd/run/siw_lgn", 
    ),
    array(
      "title" => "UCL WTS",
      "slug" => array("wts", "remotecluster"),
      "url" => "http://www.ucl.ac.uk/isd-extra/common/windows/wts-web/", 
    ),
    array(
      "title" => "Library Catalogue",
      "slug" => array("library", "lib"),
      "url" => "http://library.ucl.ac.uk/", 
    ),
  ),
  "Rooms" => array(
    array(
      "title" => "Find a room",
      "slug" => array("rooms"),
      "url" => "https://cmis.adcom.ucl.ac.uk:4443/roomBookings/bookableSpace/viewAllBookableSpace.html?invoker=EFD", 
    ),
    array(
      "title" => "Book a study room",
      "slug" => array("studyrooms","studyroom"),
      "url" => "http://library.ucl.ac.uk/F/?func=file&file_name=room-book", 
    ),
    array(
      "title" => "Campus map",
      "slug" => array("map","maps"),
      "url" => "http://www.ucl.ac.uk/locations/ucl-maps/ucl-bloomsbury-campus-map", 
    ),
  ),
  "General information" => array(
    array(
      "title" => "Term dates",
      "slug" => array("terms", "termdates"),
      "url" => "http://www.ucl.ac.uk/staff/term-dates/", 
    ),
    array(
      "title" => "Student centre",
      "slug" => array("studentcentre"),
      "url" => "http://www.ucl.ac.uk/ras/about/division/student_centre_folder/student_centre_about
      ", 
    ),
  ),
  "IT Support" => array(
    array(
      "title" => "Email (Live@UCL)",
      "slug" => array("emailsupport"),
      "url" => "http://www.ucl.ac.uk/isd/students/mail/live", 
    ),
    array(
      "title" => "Wireless (Eduroam)",
      "slug" => array("eduroam","wifi"),
      "url" => "https://www.ucl.ac.uk/isd/students/wireless", 
    ),
  ),
);



$subdomain = explode(".", $_SERVER["HTTP_HOST"]);
$uri = explode("/", $_SERVER['REQUEST_URI']);

// Redirect subdomain shortcuts
if($subdomain[0] !== 'ucl')
  foreach($LINKS as $t=>$links) 
    foreach ($links as $link)
      foreach($link['slug'] as $slug)
        if($subdomain[0] == $slug) header("Location:" . $link['url'] );

// Redirect URI shortcuts
if(isset($uri[1]) && $uri[1] !== '')
  foreach($LINKS as $t=>$links) 
    foreach ($links as $link)
      foreach($link['slug'] as $slug)
        if($uri[1] == $slug) header("Location:" . $link['url'] );

?>





<!-- 
TEMPLATE
========
-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN">

<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="http://www.facebook.com/2008/fbml"
      lang="en-GB">
<head>
  <meta charset="UTF-8">
  <title>UCL.me - University College London Quick Links</title>
  <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.3.0/build/cssreset/reset-min.css">
  <link rel="icon" type="image/gif" href="/assets/favicon.gif" />
  <meta property="og:site_name" content="UCL.me - University College London Quick Links"/>
  <meta property="og:description" content="Quick access to all useful UCL links!"/>
  <meta property="og:title" content="UCL.me - University College London Quick Links"/>
  <meta property="og:url" content="http://ucl.me"/>
  <meta property="og:locale" content="en_GB"/>
  <!-- 
  MOBILE BROWSER STUFF
  -->
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
  <meta name="description" content="Quick access to all useful UCL links" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/icon-ipad.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/icon-iphone4.png" />
  <link rel="apple-touch-icon-precomposed"  href="/assets/icon-iphone.png" />
  
  
  <!-- 
  MAIN STYLES
  -->
  <style type="text/css">
    html {font-family: "Helvetica Neue", Roboto, Helvetica, Arial, sans-serif; background:#ddd; }
    body {overflow-x:hidden;}
    a, a:visited {color:#0086d3;}
    #header {background:#45484d; border-bottom:1px solid #45484d;height:40px;overflow:visible;position:relative;}

    #header h1 {font-weight:bold; padding:5px 10px; color:white; font-size:20px; text-shadow:black 0px 0px 1px;float:left;}
    #header h1 span {font-weight:normal;font-size:12px;line-height:20px;}
    #main {background:#eee; border-bottom:1px solid #ccc; padding:10px; overflow:hidden;}
    #main .fb-like {height:40px;overflow:visible;}
    #main h2 {color:#444; letter-spacing:1px; text-transform:uppercase; padding:5px; font-size:11px; text-shadow:1px 1px 0px white;
      border-bottom:1px solid #ddd;}
    #main .category ul {overflow:auto; clear:both;margin:10px 0px;}
    #main .category ul li {float:left;margin:0 10px 10px 0px;}
    #main .category ul li a {text-decoration:none;font-size:.8em;}
    #main .category ul a .image {width:120px; height:120px; display:table-cell;background:white; text-align:center; vertical-align:middle;border:1px solid #ccc;
      border-radius:10px;}
    #main .category ul a .title {color:gray; display:block; text-align:center; padding:5px;}
    #main .category ul a:hover .image {border:1px solid gray;}
    #main .category ul a:hover .title {color:#111;}
    #footer {font-size:.8em; color:gray;width:100%;}
    #footer p {padding:10px;}
    .clear {clear:both;}

    /* Style changes for mobile devices*/
    @media screen and (max-width: 480px) {
    }

    /* Gradients */
    #header {
    background: #45484d; /* Old browsers */
    background: -moz-linear-gradient(top, #45484d 0%, #000000 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#45484d), color-stop(100%,#000000)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, #45484d 0%,#000000 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, #45484d 0%,#000000 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, #45484d 0%,#000000 100%); /* IE10+ */
    background: linear-gradient(top, #45484d 0%,#000000 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#45484d', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
    }

  </style>
</head>
<body>
<header>
<div id="header">
  <h1>UCL.me&nbsp;&nbsp;<span>University College London Quick Links</span></h1>
</div>
</header>
<div id="main">
  <div class="fb-like" data-href="http://facebook.com/ucl.me" data-send="false" data-width="450" data-show-faces="false" data-colorscheme="light" data-font="arial"
 data-layout="button_count" ></div>
  <div class="clear"></div>
  <? foreach($LINKS as $category => $links):?>
  <div class="category">
    <h2><?=$category?></h2>
    <ul id="links">
      <? foreach($links as $link):?>
        <li><a href="<?=$link['url']?>" title="Shortcut: <?=$link['slug'][0]?>.ucl.me">
          <? if(isset($link['icon']) && !$link['icon']): ?>
            <span class="image"><?=$link['slug'][0]?>.ucl.me</span>
          <? else: ?>
            <span class="image" style="background-image:url('/assets/<?=$link['slug'][0]?>.jpg')"></span>
          <? endif; ?>
          <span class="title"><?=$link['title']?></span>
          </a></li>
      <? endforeach; ?>
    </ul>
  </div>
  <? endforeach; ?>
  <p style="padding:20px; font-size:.8em;"><strong>&hellip;don't forget to bookmark us! ;-)</strong></p>
</div>
<footer>
  <div id="footer">
    <p>UCL.me is not afilliated with University College London
    - <a href="https://docs.google.com/document/pub?id=1fHycu1-hDp-Z470J8BJMCveLB4LBe2XOP-9dFjvI32s">About</a>
    - <a href="https://docs.google.com/spreadsheet/viewform?formkey=dFFnWVc0cDlRdmhaRjRmMThfQXpjTmc6MQ">Suggest link</a></p>
  </div>
</footer>


<!-- 
FACEBOOK LIKE
=============
-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=328614740488918";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>



<!-- 
GOOGLE ANALYTICS
================
-->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-74600-15']);
  _gaq.push(['_setDomainName', 'ucl.me']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</body>
</html>
